# JW Player 7 Listbar Example

JW Player 7 listbar example build on top of [Listy plugin](http://dev.powered-by-haiku.co.uk/solutions/listy/).

![screenshot](assets/jwplayer-listbar-listy-demo.png)

### How to run local demo server

```
npm install
```

```
npm run demo
```

Open url http://localhost:5000/listbar in browser.
